describe http('http://127.0.0.1:9216/metrics') do
  its('status') { should eq 200 }
  its('body') { should match /mongodb_up 1/ }
end
