## For versions of Mongo that use the YAML format for configuration, use the
## following. All entries in mongod_settings are written to the config file
## verbatim. The storage:dbPath and systemLog:path entries are required in
## this usage and take precedence over db_path at the top level (see references
## in mongodb/init.sls).



## Use this for MongoDB 3.6 on Debian
mongodb:
  use_repo: True
  version: 3.6
  keyid: 58712A2291FA4AD5
  mongodb_package: mongodb-org
  conf_path: /etc/mongod.conf
  log_path: /srv/mongodb/log
  db_path: /srv/mongodb/data
  mongod: mongod
  replicaset:
    key: blablablatestkey
    keyfile: &keyfile /srv/mongodb/keyfile
  mongod_settings:
    systemLog:
      destination: file
      logAppend: true
      path: /var/log/mongodb/mongod.log
    storage:
      dbPath: /srv/mongodb/data
      journal:
        enabled: true
    net:
      port: 27017
      bindIp: 0.0.0.0
      # This entire configuration block is required to enable TLS in the
      # MongoDB server
      #ssl:
      #  mode: requireSSL
      #  PEMKeyFile: /srv/certs/{{ grains['id'] }}.bundle.pem
      #  CAFile: /srv/certs/bundleca.pem
      #  allowConnectionsWithoutCertificates: true
    setParameter:
      # This parameter allows the configuration of the admin user.
      # As soon as the admin user is configured, mongodb will no longer
      # allow this to happen.
      enableLocalhostAuthBypass: true
    security:
      authorization: enabled
      keyFile: *keyfile
    replication:
      replSetName: rs1
  users:
    # The admin user is special and it is provisioned before any other users.
    admin:
      # We don't want a user check to be executed for the admin user because
      # such a check requires the admin user to be present. Chicken and egg
      # problem.
      - check: False
      - passwd: password
      - roles:
        - role: root
          db: admin
    # All commands under the username are arguments for the mongodb_user state
    # and, as such, require a list format.
    test:
      - passwd: testpassword
      - roles:
        - role: read
          db: reporting
        - role: readWrite
          db: test
