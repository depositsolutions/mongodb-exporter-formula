mongodb_exporter:
  bin_dir: '/usr/bin'
  dist_dir: '/usr/lib/mongodb_exporter/dist'
  version: '0.6.1'
  service: True
  exporter_user: 'admin'
  exporter_password: 'password'
  tls: False
