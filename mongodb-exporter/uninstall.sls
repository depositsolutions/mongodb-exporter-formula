{% from slspath+"/map.jinja" import mongodb_exporter with context %}

mongodb_exporter-remove-service:
  service.dead:
    - name: mongodb_exporter
  file.absent:
    - name: /etc/systemd/system/mongodb_exporter.service
    - require:
      - service: mongodb_exporter-remove-service
  cmd.run:
    - name: 'systemctl daemon-reload'
    - onchanges:
      - file: mongodb_exporter-remove-service

mongodb_exporter-remove-symlink:
   file.absent:
    - name: {{ mongodb_exporter.bin_dir}}/mongodb_exporter
    - require:
      - mongodb_exporter-remove-service

mongodb_exporter-remove-binary:
   file.absent:
    - name: {{ mongodb_exporter.dist_dir}}
    - require:
      - mongodb_exporter-remove-symlink

mongodb_exporter-remove-env:
    file.absent:
      - name: {{ mongodb_exporter.config_dir }}
      - require:
        - mongodb_exporter-remove-binary
