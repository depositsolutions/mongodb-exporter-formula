{% from slspath+"/map.jinja" import mongodb_exporter with context %}

mongodb_exporter-install-service:
  file.managed:
    - name: /etc/systemd/system/mongodb_exporter.service
    - source:  salt://{{ slspath }}/files/mongodb_exporter.service.j2
    - template: jinja
    - context:
        mongodb_exporter: {{ mongodb_exporter }}

mongodb_exporter-service:
  service.running:
    - name: mongodb_exporter
    - enable: True
    - watch:
      - file: mongodb_exporter-install-service

{% if mongodb_exporter.exporter_password is defined %}
mongodb_exporter-create-env:
  file.managed:
    - name: /etc/systemd/system/mongodb_exporter.service.d/env.conf
    - source: salt://{{ slspath }}/files/env.conf.j2
    - makedirs: True
    - template: jinja
    - context:
        mongodb_exporter: {{ mongodb_exporter }}
    - user: root
    - group: root
    - mode: 644
  module.run:
  - name: service.systemctl_reload
  - onchanges:
    - file: /etc/systemd/system/mongodb_exporter.service.d/env.conf
{% endif %}
